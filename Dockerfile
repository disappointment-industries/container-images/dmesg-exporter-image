FROM alpine
ARG TARGETARCH
WORKDIR /app
EXPOSE 9000
COPY dmesg_exporter-${TARGETARCH} /app/dmesg_exporter
ENTRYPOINT [ "/app/dmesg_exporter" ]
CMD [ "start", "--path=/metrics" ]
